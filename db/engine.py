from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import settings

engine = create_engine(f'postgresql://{settings.DATABASE_USER}:{settings.DATABASE_USER_PASSWORD}@{settings.DATABASE_HOST}:{settings.DATABASE_PORT}/fastapi')
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)