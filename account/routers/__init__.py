from .router import router as account_router

routers = [
    account_router,
]

__all__ = routers
