from app.base.views import CRUDBaseModelRouter
from pydantic import BaseModel


class AccountModel(BaseModel):
    id: int
    name: str
    email: str
    name: str
    surname: str
    patronymic: str
    update_number: int


class AccountRouter(CRUDBaseModelRouter):
    model = AccountModel
    path = 'account'


router = AccountRouter()

__all__ = [
    "router",
]
