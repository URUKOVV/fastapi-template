from app.base.models import BaseModel
from sqlalchemy import Column, String


class UserAccountModel(BaseModel):
    __tablename__ = "user_accounts"

    username = Column(String(length=128), name='username', nullable=False, index=True, unique=True)
    email = Column(String, name='email', nullable=False, index=True, unique=True)
    password = Column(String(length=256), name='password', nullable=False)
    _name = Column(String(length=128), name='name', nullable=False)
    _surname = Column(String(length=128), name='surname', nullable=False)
    _patronymic = Column(String(length=128), name='patronymic', nullable=True)

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'

    # @property
    # def name(self):
    #     return self._name
    #
    # @name.setter
    # def name(self, value):
    #     if len(value) > self._name.type.length:
    #         raise ValueError("Name is too long. Max length is 128")
    #     self._name = value
    #
    # @property
    # def surname(self):
    #     return self._surname
    #
    # @surname.setter
    # def surname(self, value):
    #     if len(value) > self._surname.type.length:
    #         raise ValueError("Surname is too long. Max length is 128")
    #     self._surname = value
    #
    # @property
    # def patronymic(self):
    #     return self._patronymic
    #
    # @patronymic.setter
    # def patronymic(self, value):
    #     if len(value) > self._patronymic.type.length:
    #         raise ValueError("Patronymic is too long. Max length is 128")
    #     self._patronymic = value

    def __str__(self):
        return ''


__all__ = ["UserAccountModel",]
