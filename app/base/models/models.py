from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer


class Base(object):
    id = Column(Integer, name='id', nullable=False, primary_key=True, autoincrement=True)
    update_number = Column(Integer, name='update_number', nullable=False, default=1)

    def __repr__(self):
        return f"<{str(self.__class__)} (id={self.id}, update_number={self.update_number})>"


BaseModel = declarative_base(cls=Base)
