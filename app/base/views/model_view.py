from fastapi import APIRouter
from pydantic import BaseModel


class CRUDBaseModelRouter(APIRouter):
    path: str = None
    model: type(BaseModel) = None
    prefix: str = '/api'

    def __init__(
            self,
            *args,
            **kwargs
    ):
        super().__init__(*args, prefix=self.prefix, **kwargs)

        if self.path is None:
            raise AttributeError('path should be set')
        if self.model is None:
            raise AttributeError('model should be set')

        self.add_api_route(
            f'/{self.path}/' + '{pk}',
            endpoint=self.get_instance,
            methods=["GET"],
            response_model=self.model,
            responses={404: {"description": "Not found."}}
        )
        self.add_api_route(
            f'/{self.path}/', endpoint=self.get_list, methods=["GET"], response_model=self.model
        )
        self.add_api_route(
            f'/{self.path}/' + '{pk}',
            endpoint=self.save_instance,
            methods=["PUT"],
            responses={404: {"description": "Not found"}}
        )
        self.add_api_route(
            f'/{self.path}/', endpoint=self.create_instance, methods=["POST"]
        )

    async def get_instance(self, pk: int):
        pass

    async def get_list(self):
        pass

    async def save_instance(self, pk: int, instance: BaseModel):
        pass

    async def create_instance(self, instance: BaseModel):
        pass
