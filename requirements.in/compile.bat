@echo off

pushd %~dp0

if /I "%1%" EQU "upgrade" goto upgrade

for %%f in (*.in) do (
    pip-compile -q --allow-unsafe --output-file ..\requirements\%%~nf.txt %%~f || goto error
)