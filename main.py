from fastapi import FastAPI
import settings
import importlib

app = FastAPI()

for routers in settings.ROUTERS:
    module = importlib.import_module(routers)
    for router in getattr(module, 'routers'):
        app.include_router(router)
