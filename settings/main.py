import os

DATABASE_HOST = os.environ.get("DATABASE_HOST", "127.0.0.1")
DATABASE_PORT = os.environ.get("DATABASE_PORT", "5432")
DATABASE_USER = os.environ.get("DATABASE_USER", "postgres")
DATABASE_USER_PASSWORD = os.environ.get("DATABASE_USER_PASSWORD", "0000")
DATABASE_NAME = os.environ.get("DATABASE_NAME", "fastapi")

INSTALLED_APPS = [
    'account',
    'app'
]

ROUTERS = [
    'account.routers'
]